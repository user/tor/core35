#!/usr/bin/env python

import sys

metrics = {}

fontmap = {
	"AvantGarde-Book": "URWGothicL-Book",
	"AvantGarde-BookOblique": "URWGothicL-BookObli",
	"AvantGarde-Demi": "URWGothicL-Demi",
	"AvantGarde-DemiOblique": "URWGothicL-DemiObli",

	"Bookman-Light": "URWBookmanL-Ligh",
	"Bookman-LightItalic": "URWBookmanL-LighItal",
	"Bookman-Demi": "URWBookmanL-DemiBold",
	"Bookman-DemiItalic": "URWBookmanL-DemiBoldItal",

	"Courier": "NimbusMonL-Regu",
	"Courier-Oblique": "NimbusMonL-ReguObli",
	"Courier-Bold": "NimbusMonL-Bold",
	"Courier-BoldOblique": "NimbusMonL-BoldObli",

	"Helvetica": "NimbusSanL-Regu",
	"Helvetica-Oblique": "NimbusSanL-ReguItal",
	"Helvetica-Bold": "NimbusSanL-Bold",
	"Helvetica-BoldOblique": "NimbusSanL-BoldItal",

	"Helvetica-Condensed": "NimbusSanL-ReguCond",
	"Helvetica-Condensed-Oblique": "NimbusSanL-ReguCondItal",
	"Helvetica-Condensed-Bold": "NimbusSanL-BoldCond",
	"Helvetica-Condensed-BoldObl": "NimbusSanL-BoldCondItal",

	"NewCenturySchlbk-Roman": "CenturySchL-Roma",
	"NewCenturySchlbk-Italic": "CenturySchL-Ital",
	"NewCenturySchlbk-Bold": "CenturySchL-Bold",
	"NewCenturySchlbk-BoldItalic": "CenturySchL-BoldItal",

	"Palatino-Roman": "URWPalladioL-Roma",
	"Palatino-Italic": "URWPalladioL-Ital",
	"Palatino-Bold": "URWPalladioL-Bold",
	"Palatino-BoldItalic": "URWPalladioL-BoldItal",

	"Times-Roman": "NimbusRomNo9L-Regu",
	"Times-Italic": "NimbusRomNo9L-ReguItal",
	"Times-Bold": "NimbusRomNo9L-Medi",
	"Times-BoldItalic": "NimbusRomNo9L-MediItal",

	"Symbol": "StandardSymL",
	"ZapfDingbats": "Dingbats",
	"ZapfChancery-MediumItalic": "URWChanceryL-MediItal",
}

def load_afm(filename):
	name = "unknown"
	mtx = {}
	for line in open(filename).readlines():
		if line.startswith("FontName"):
			line = line.split()
			name = line[1]
		elif line.startswith("C "):
			line = line.split()
			w = int(line[4])
			n = line[7]
			mtx[n] = w
	metrics[name] = mtx

def diff_mtx(afont, ufont):
	amtx = metrics[afont]
	umtx = metrics[ufont]
	aset = amtx.keys()
	uset = umtx.keys()
	for name in aset:
		if name not in uset:
			print "missing %s/%s (urw) %s" % (afont, ufont, name)
		elif amtx[name] != umtx[name]:
			print "width %s/%s %s adobe=%d urw=%d" % (afont, ufont, name, amtx[name], umtx[name])
	for name in uset:
		if name not in aset:
			print "missing %s/%s (adobe) %s" % (afont, ufont, name)

for afm in sys.argv[1:]:
	load_afm(afm)

for font in metrics.keys():
	if font in fontmap:
		ufont = fontmap[font]
		if ufont in metrics:
			diff_mtx(font, ufont)
