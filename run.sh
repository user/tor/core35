#!/bin/sh

rm -rf cff otf ttf accented
mkdir -p cff otf ttf accented

fontforge -script convert.pe type1/*.pfb
fontforge -script convert12.pe type1/n0*.pfb
rm accented/*Cond*
python unwrap.py cff/*.cff accented/*.cff

