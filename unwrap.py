#!/usr/bin/env python
#
# Strip off the stupid postscript prolog and epilog that fontforge
# wraps bare cff fonts with.
#

import sys

def unwrap(filename):
	f = open(filename, "rb")
	line = f.readline()
	if not line.startswith("%!PS-Adobe-3.0 Resource-FontSet"):
		print filename, "not wrapped"
		return
	while 1:
		line = f.readline()
		if line.endswith("StartData\n"):
			break
		if not line:
			print filename, "cannot find StartData"
			return

	a, b, c = line.split()
	size = int(b)
	data = f.read(size)
	f.close()
	print a, size

	f = open(filename, "wb")
	f.write(data)
	f.close()

for filename in sys.argv[1:]:
	unwrap(filename)

